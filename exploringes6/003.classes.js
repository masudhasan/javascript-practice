class Point{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }

    toString(){
        return `(${this.x}, ${this.y})`;
    }
}

class ColorPoint extends Point{
    constructor(x,y,c){
        super(x,y);
        this.c = c;
    }

    toString(){
        return  super.toString() + ' in ' +  this.c;
    }
}

var point1 = new Point(1,4);
console.log(point1.toString());

var colorPoint1 = new ColorPoint(1,4,'red');
console.log(colorPoint1.toString());

console.log(typeof Point);














