class Foo{
    constructor(prop){
        this.prop = prop;
    }

    static staticMethod(){
        return "classy";
    }

    prototypeMethod(){
        return "prototype method";
    }

}
console.log(Foo === Foo.prototype.constructor);


