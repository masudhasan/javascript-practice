
/////////////////////////////   let and const //////////////////////////////////////
// var has a function scope
for(var i = 0 ; i <= 9 ; i++){
}
console.log(i);


for(let j = 0 ; j <= 9 ; j++){
}
//console.log(j); //ReferenceError

const x = 10;
//x = 11;// TypeError
console.log(x);


// IEFE to block scope
(function(){
    var a = 100;
})();
//console.log(a); // ReferenceError

{
    let b = 200;
}
//console.log(b); // ReferenceError

///////////////////////////////////////////////////////////////////////////////

///////////////////////  template literal ////////////////////////////////////
function greet(name) {
    console.log(`Hello ${name}`);
}

greet("masud");
/////////////////////////////////////////////////////////////////////////////

////////////////////// multi line string ////////////////////////////////////
var multiLine = `this is line 1
this is line 2
this is line 3`
console.log(multiLine);
///////////////////////////////////////////////////////////////////////////

/////////////////////// arrow function //////////////////////////////////
var sum = (a,b) => a+b;
let result = sum(2,5);
console.log(result);

const numbes = [2,3,4];
const snumers = numbes.map(x => x * x);
console.log(snumers);
//////////////////////////////////////////////////////////////////////////////////

//////////////////// destructing ////////////////////////////////////////////////

var hobbies = ['reading' , 'football' , 'tennis'];
const [firstHobby , secondHobby] = hobbies;
console.log(firstHobby);//reading

var myObj = {
    x : 100,
    y : 200
};
const {x:arg1 , y:arg2} = myObj;
console.log(arg1); // 100

// destructing helps processing return values
const obj = {foo : 123};
var {writable , configurable} = Object.getOwnPropertyDescriptor(obj, 'foo');
console.log(writable, configurable); // true true

const [, year, month, day] =
    /^(\d\d\d\d)-(\d\d)-(\d\d)$/
    .exec('2999-12-31');
console.log(year)//2999

/////////////////////////////////////////////////////////////////////////////////

/////////////////////// for each , for of and for in loop //////////////////////////////
let fruits = ['apple','orange','grape'];
fruits.forEach(fruit => console.log( `I love ${fruit}`));

for(let fruit of fruits){
    console.log( `I love ${fruit}`);
}

var person = {
    name :'masud hasan',
    age : 30
};

for(let key in person){
    let value = person[key];
    console.log(`${key} -> ${value}`);
}
/////////////////////////////////////////////////////////////////////////////////////
////////////////////// parameter default values ////////////////////////////////////
//old way of doing it
function greetPerson(greet,name){
    greet = greet || "Hello";
    name = name || "Guest";
    console.log(`${greet} ${name}`);
}
greetPerson();

//new way
function greetGuest(greet = "Hello",name = "Guest"){
    console.log(`${greet} ${name}`);
}
greetGuest();
//////////////////////////////////////////////////////////////////////////////////////

//////////////////// rest parameter ////////////////////////////////////////////////
function func(...args){
    for(let arg of args){
        console.log(arg);
    }
}
func("amsterdam",'delhi');
///////////////////////////////////////////////////////////////////////////////////

///////////////////// spread parameneter /////////////////////////////
console.log(Math.max(100,3,455,65));
console.log(Math.max.apply(Math,[100,3,455,65]));

//converts array to arguments
console.log(Math.max(...[100,3,455,65]));


const argx = [1,2,3];
const argy = [4,5,6];
const argz = [7,8,9];
console.log(...argx,...argy,...argz);

argx.push(...argy);
console.log(argx);
////////////////////////////////////////////////////////////////////////////////////

/////////////// function literal /////////////////////////////////////////////////
var car = {
    brand : "BMW",
    price : 1000000,
    calculatePrice(){
        return `price of ${this.brand} is ${this.price}`;
    }
};
console.log(car.calculatePrice());
/////////////////////////////////////////////////////////////////////////////////////

///////////////////////// froim contructor function to classes ///////////////////////////////
//ES5 way
function Student(name){
    this.name = name;
}
Student.prototype.describe = function(){
    return `student is called ${this.name}`;
}
var student1 = new Student("Masud Hasan");
console.log(student1.describe());

//ES 6 way
class Author{
    constructor(name){
        this.name = name;
    }

    describe(){
        return `author is called ${this.name}`
    }
}
const author1 = new Author('Rabimdranath Tagore');
console.log(author1.describe());
////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// derived calsses ///////////////////////////////
class People {
    constructor(name){
            this.name = name;
    }
    getName(){
        return this.name;
    }
}

class Driver extends People{
    constructor(name,drives){
        super(name);
        this.drives = drives;
    }
    describe(){
        return `${this.getName()} drives ${this.drives}`;
    }
}
const driver1 = new Driver('Hamilton','Mercedes');
console.log(driver1.describe());
/////////////////////////////////////////////////////////////////////////////////






































































































































