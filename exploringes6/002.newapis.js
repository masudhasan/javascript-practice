//You can now specify integers in binary and octal notation:
var binary = 0b11;
console.log(binary);
var octal = 0o10;
console.log(octal);

//////////////////////////////////////////////////////////////////
/// iterator over string ////////////////////////////////////////
for(ch of 'masud'){
    console.log(ch);
}

const rdoo = "doo".repeat(3);
console.log(rdoo);

///////////// symbols //////////////////////////////////////////
let mySumbol = Symbol('this is a new symbol');
console.log(mySumbol);

console.log(
    typeof(class {})
);


////////////////////// arrow function /////////////////////
const sa = [1,2,3].map(x => x * 2);
console.log(sa);

const x = (() => 5)();
console.log(x);

console.log(typeof(() => 5));
